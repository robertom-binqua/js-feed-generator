const express = require('express')
const loadConfiguration = require('./configurationLoader')
const log4js = require('log4js');
require('./log4jsInitialisation')
const logger = log4js.getLogger("socketIOServer");
const feedStateFactory = require('./feedStateFactory')(log4js.getLogger("feedState"));

let configuration = loadConfiguration(process.argv);

if (!configuration.isValid) {
    console.log(configuration.message)
} else {

    const {port, token, emissionIntervalInMills} = configuration.config

    const app = express()

    app.use(express.urlencoded({extended: true}))

    const server = require('http').createServer(app)

    let feedState = feedStateFactory.nonConnectedFeedState()

    app.post('/subscribe', function (req, res) {
        logger.info(`Incoming post request ${req.url}`)
        if (!feedState.isConnected) {
            res.send('Socket not connected. Please connect first')
            return
        }
        if (req.get('content-type') !== 'application/x-www-form-urlencoded') {
            res.send("content-type has to be 'application/x-www-form-urlencoded' not " + req.get('content-type'))
            return
        }
        const expAuth = `Bearer ${feedState.socketId}${token}`
        if (req.get('Authorization') !== expAuth) {
            res.send(`Authorization has to be ${expAuth} not ${req.get('Authorization')}`)
            return
        }
        if (req.get('Content-Language') !== 'en-US') {
            res.send("Content-Language has to be en-US not " + req.get('Content-Language'))
            return
        }
        if (req.originalUrl !== "/subscribe") {
            res.send("Post to subscribe has to be /subscribe only not " + req.originalUrl)
            return
        }
        if (req.get("path") !== "/subscribe") {
            res.send("Header path has to be 'subscribe' not " + req.get("path"))
            return
        }
        if (parseInt(req.get("port")) !== port) {
            res.send("Header port has to be " + port + ". " + req.get("port") + " is wrong")
            return
        }
        if (req.accepts('application/json') !== 'application/json') {
            res.send("request must accept application/json")
            return
        }
        if (!req.body.pairs) {
            res.send("request body has to start with pairs=. " + req.body + " is wrong")
            return
        }

        logger.info(`Try to subscribe ${req.body.pairs}`)

        const subscriptionResult = feedState.subscribe(req.body.pairs)

        if (subscriptionResult.successfulSubscription) {
            logger.info(`${req.body.pairs} http subscription part ok.`)
        } else {
            logger.info(`${req.body.pairs} http subscription failed`)
        }

        res.send(subscriptionResult.message)

    })

    const io = require('socket.io')(server, {
        pingInterval: 2000,
        pingTimeout: 100,
        wsEngine: 'ws'
    });

    server.listen(port, function () {
        logger.info(`Socket.IO server listening on port = ${port} with token = ${token} with emissionIntervalInMills = ${emissionIntervalInMills}`);
    });

        io.of("/").on('connection', function (socket) {

        logger.info(`Client connected. Socket id ${socket.id}`)

        feedState = feedStateFactory.connectedFeedState(token, socket, emissionIntervalInMills)

        socket.on('disconnect', (reason) => {
            if (reason === 'server namespace disconnect') {
                logger.info('server disconnected');
            } else {
                logger.info("client request disconnection")
            }
            feedState.stop()
        });

        socket.on('error', function () {
            console.log('error: ', arguments);
        });

        socket.on('getHandshake', function (cb) {
            cb(socket.handshake);
        });

    });

    function before(context, name, fn) {
        const method = context[name];
        context[name] = function () {
            fn.apply(this, arguments);
            return method.apply(this, arguments);
        };
    }

    before(io.engine, 'handleRequest', function (req, res) {
        logger.info(`Incoming request ${req.url}`)

        if (feedState.validateUrlRequest) feedState.validateUrlRequest(req.url)

        const value = req.headers['x-socketio'];
        if (!value) return;
        res.setHeader('X-SocketIO', value);
    });

    before(io.engine, 'handleUpgrade', function (req) {
        const value = req.headers['x-socketio'];
        if (!value) return;
        this.ws.once('headers', function (headers) {
            headers.push('X-SocketIO: ' + value);
        });
    });

    let signals = {
        'SIGINT': 2,
        'SIGTERM': 15
    };

    function shutdown(signal, value) {
        if (feedState.stop) {
            feedState.stop()
        }

        io.close(function () {
            logger.info(`server stopped by ${signal}`);
            //https://tldp.org/LDP/abs/html/exitcodes.html
            process.exit(128 + value);
        })
    }

    Object.keys(signals).forEach(function (signal) {
        process.on(signal, function () {
            shutdown(signal, signals[signal]);
        });
    });

}


