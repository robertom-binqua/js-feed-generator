const commandLineArgs = require('command-line-args')
const commandLineUsage = require('command-line-usage')
const immer = require('immer')

const optionDefinitions = [
    {name: 'help', type: Boolean, alias: 'h'},
    {name: 'port', type: Number, alias: 'p'},
    {name: 'token', type: String, alias: 't'},
    {name: 'emissionIntervalInMills', type: Number, alias: 'i'},
]

const helpGuideSection = (optionDefinitions) => ([
    {
        header: 'Mocked Feed Server',
        content: 'Starts a mock feed server and as soon a connection is established and a subscription is successful it starts emitting quotes.'
    },
    {
        header: 'Options',
        optionList: optionDefinitions
    }
])

const defaultConfig = {
    port: 3000,
    token: "thisIsTheTokenNow",
    emissionIntervalInMills: 500,
}

const toConfig = (defaultConfig, readConfig) => {
    return immer.produce(defaultConfig, newConfigEntry => {
        if (readConfig.port) {
            newConfigEntry.port = readConfig.port
        }
        if (readConfig.token) {
            newConfigEntry.token = readConfig.token
        }
        if (readConfig.emissionIntervalInMills) {
            newConfigEntry.emissionIntervalInMills = readConfig.emissionIntervalInMills
        }
    })
}

const loadConfiguration = (function (defaultConfig, optionDefinitions, helpGuideSection) {
    return incomingArgs => {
        function helpConfig(message) {
            return (Object.freeze({
                isValid: false,
                message: message,
                config: null
            }));
        }

        function validConf(config) {
            return (Object.freeze({
                isValid: true,
                message: "",
                config: config
            }));
        }

        function invalidConfig(e, commandLineUsage) {
            return (Object.freeze({
                isValid: false,
                message: `${e.message}\n${commandLineUsage}`,
                config: null
            }))
        }

        try {
            const parsedArgs = commandLineArgs(optionDefinitions, {argv: incomingArgs})
            return (parsedArgs.help) ? helpConfig(commandLineUsage(helpGuideSection)) : validConf(toConfig(defaultConfig, parsedArgs))
        } catch (e) {
            return invalidConfig(e, commandLineUsage(helpGuideSection))
        }

    }
}(defaultConfig, optionDefinitions, helpGuideSection(optionDefinitions)))


module.exports = loadConfiguration
