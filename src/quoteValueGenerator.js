const {upAndDownCounter, formatter, precisionOf} = require("./counter")

const quoteValueGenerator = (config) => {
    let counter = upAndDownCounter(config);
    return () => formatter(counter(), precisionOf(config.initialValue))
}

module.exports = quoteValueGenerator
