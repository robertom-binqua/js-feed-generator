const upAndDownCounter = ({initialValue, numberOfValues, increment}) => {
    const startingValue = removeCommaFrom(initialValue)
    const highestValue = startingValue + (numberOfValues - 1) * increment
    let nextValue = startingValue
    let directionUp = true

    const goUp = (value, increment) => value + increment
    const goDown = (value, increment) => value - increment

    return () => {
        const returningValue = nextValue
        if (directionUp) {
            if (returningValue < highestValue) {
                nextValue = goUp(returningValue, increment)
            } else {
                nextValue = goDown(returningValue, increment)
                directionUp = !directionUp
            }
        } else {
            if (returningValue > startingValue) {
                nextValue = goDown(returningValue, increment)
            } else {
                nextValue = goUp(returningValue, increment)
                directionUp = !directionUp
            }
        }
        return returningValue
    }
}

const formatter = (toBeFormatted, decimalPosition) => {
    const stringToBeFormatted = toBeFormatted.toString()
    if (decimalPosition >= stringToBeFormatted.length) {
        let zerosPrefix = `${"0".repeat(decimalPosition - stringToBeFormatted.length)}`;
        return `0.${zerosPrefix}${stringToBeFormatted}`
    } else {
        const integerPart = stringToBeFormatted.substr(0, stringToBeFormatted.length - decimalPosition)
        const decimalPart = stringToBeFormatted.substr(stringToBeFormatted.length - decimalPosition, stringToBeFormatted.length)
        return `${integerPart}.${decimalPart}`
    }
}

const precisionOf = aString => {
    if (aString.indexOf(".") === -1) {
        return 0
    }
    return aString.substr(aString.indexOf(".") + 1, aString.length).length
}

const plusPips = (initialValue, pips, formatter) => formatter(removeCommaFrom(initialValue) + pips, precisionOf(initialValue))

const removeCommaFrom = (initialValue) => {
    const precision = precisionOf(initialValue)
    const decimalPart = initialValue.substr(initialValue.length - precision, initialValue.length)
    const integerPart = initialValue.substr(0, initialValue.indexOf("."))
    return integerPart * Math.pow(10, precision) + parseInt(decimalPart)
}

module.exports = {upAndDownCounter, formatter, precisionOf, plusPips}
