const yup = require('yup');

class FeedState {

    timerIdByCurrencyPair = new Map()

    constructor(config, feedIncomingQuoteGenerator, quoteDecorator, socket, logger, token) {

        const validate = (configToBeValidated) => {

            const singleConfigEntrySchema = yup.object().strict(true).shape({
                sell: yup.string().matches(/\d+\.\d+/g),
                numberOfValues: yup.number().required().positive().integer(),
                increment: yup.number().required().positive().integer(),
                pair: yup.string().required().min(4).strict(true),
                feedEmissionIntervalMills: yup.number().required().positive().min(100).integer(),
            });

            yup.array()
                .min(1)
                .of(singleConfigEntrySchema)
                .validateSync(configToBeValidated);
        }

        validate(config)

        this.config = config;

        this.quoteDecorator = quoteDecorator;
        this.feedIncomingQuoteGenerator = feedIncomingQuoteGenerator
        this.socket = socket
        this.token = token
        this.urlIsValid = false

        if (!socket) {
            throw Error("Socket cannot be null")
        }
        this.logger = logger

    }

    validateUrlRequest(urlToBeValidated) {
        const validation = (url) => {
            if (url.startsWith("/socket.io/?")) {
                const simpleUrl = urlToBeValidated.replace("/socket.io/?", "")
                const urlComponents = simpleUrl.split("&")
                return urlComponents.includes(`access_token=${this.token}`) &&
                    urlComponents.includes(`EIO=3`) &&
                    (urlComponents.includes("transport=polling") || urlComponents.includes("transport=websocket")) &&
                    urlComponents.includes(`sid=${this.socket.id}`)
            } else {
                return false
            }

        }
        if (!this.socket) {
            return false
        }

        this.urlIsValid = validation(urlToBeValidated)

        this.logger.info(`url ${urlToBeValidated} has ${this.urlIsValid ? "" : "not "}been validated successfully`)

        return this.urlIsValid
    }

    subscribe(currencyPair) {
        if (!this.socket) {
            throw Error("Feed has been stopped!")
        }

        if (!this.urlIsValid) {
            return {
                successfulSubscription: false,
                message: `url has not been validated`
            }
        }

        const findConfiguration = (currencyPair) => {
            const filtered = this.config.filter(entry => entry.pair === currencyPair);
            if (filtered)
                return filtered[0]
            else
                return false
        }

        const currencyPairConfigFound = findConfiguration(currencyPair)

        if (currencyPairConfigFound) {
            const nextQuoteFactory = this.feedIncomingQuoteGenerator(currencyPairConfigFound)
            const timerId = setInterval(() => {
                let value = this.quoteDecorator(nextQuoteFactory());
                if (this.socket) {
                    this.socket.emit(currencyPair, value);
                }
            }, currencyPairConfigFound.feedEmissionIntervalMills);
            this.timerIdByCurrencyPair.set(currencyPair, timerId)
            return {
                successfulSubscription: true,
                message: `{"response":{"executed":true,"error":""},"pairs":[${this.quoteDecorator(nextQuoteFactory())}]}`
            }
        }
        return {
            successfulSubscription: false,
            message: `Currency pair ${currencyPair} is not configured`
        }
    }

    stop() {
        if (this.socket) {
            this.logger.info(`Socket with id ${this.socket.id} stopped`)
        }
        for (let currencyPair of this.timerIdByCurrencyPair.keys()) {
            this.logger.info(`Timer for currencyPair ${currencyPair} cancelled: emitter stopped`)
            clearTimeout(this.timerIdByCurrencyPair.get(currencyPair))
        }
        this.socket = null
    }

    isStopped() {
        return this.socket == null
    }

    get isConnected() {
        return this.socket !== null
    }

    get socketId() {
        if (this.socket) {
            return this.socket.id
        } else {
            throw Error("A stopped feedState does not have a socketId. Please reconnect it")
        }
    }

}


module.exports = {FeedState}
