const log4js = require('log4js');

log4js.configure({
    appenders: {
        out: { type: 'stdout' },
        app: { type: 'file', filename: 'mockedFeedServer.log' }
    },
    categories: {
        default: { appenders: [ 'out', 'app' ], level: 'info' },
    }
});
