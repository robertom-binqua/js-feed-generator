const {plusPips, formatter} = require("./counter")
const quoteValueGenerator = require("./quoteValueGenerator")
const yup = require('yup');

const buyConfigFrom = (sellConfig) => {
    const buyValue = plusPips(sellConfig.initialValue, sellConfig.increment, formatter)
    return {...sellConfig, initialValue: buyValue}
}

const feedIncomingQuoteGenerator = config => {

    let validationSchema = yup.object().strict(true).shape({
        sell: yup.string().matches(/\d+\.\d+/g),
        numberOfValues: yup.number().required().positive().integer(),
        increment: yup.number().required().positive().integer(),
        pair: yup.string().required(),
    });

    validationSchema.validateSync(config);

    const sellConfig = {
        initialValue: config.sell,
        numberOfValues: config.numberOfValues,
        increment: config.increment,
    }

    const symbol = config.pair

    const buyConfig = buyConfigFrom(sellConfig);

    const sellValue = quoteValueGenerator(sellConfig)

    const buyValue = quoteValueGenerator(buyConfig)

    const max = plusPips(sellConfig.initialValue, (config.numberOfValues + 1) * config.increment, formatter)

    const min = sellConfig.initialValue

    return () => `{"Rates":[${sellValue()},${buyValue()},${max},${min}],"Symbol":"${symbol}"}`
}

const quoteUpdatedDecorator = (epochGenerator) => (quote) => quote.replace("{", `{"Updated":${epochGenerator()},`)

const quoteUpdatedDecoratorFactory = quoteUpdatedDecorator(() => `${Date.now()}`)

module.exports = {feedIncomingQuoteGenerator, quoteUpdatedDecorator, quoteUpdatedDecoratorFactory}
