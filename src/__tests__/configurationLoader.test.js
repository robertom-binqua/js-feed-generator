const loadConfiguration = require("../configurationLoader")
const _ = require('lodash');
const immer = require('immer')

const splited = (arrayToBeSplit) => _.flatMap(arrayToBeSplit, e => _.split(e, '='))

const customTest = (exp) => (args) => {

    let loadedConfiguration = loadConfiguration(args);

    expect(loadedConfiguration.config).toStrictEqual(exp)
    expect(loadedConfiguration.isValid).toStrictEqual(true)
    expect(loadedConfiguration.message).toBe("")
}

const toShortForm = (function () {
    const toShortFormMap = new Map()
    toShortFormMap.set("--emissionIntervalInMills", "-i")
    toShortFormMap.set("--port", "-p")
    toShortFormMap.set("--token", "-t")

    return (toBeAmmeded) => _.map(toBeAmmeded, e => toShortFormMap.has(e) ? toShortFormMap.get(e) : e)
}())

test("should read --port 3001 --token xxx  --emissionIntervalInMills 300", () => {
    const exp = {
        port: 3001,
        token: "xxx",
        emissionIntervalInMills: 300,
    }

    let args = ['--port=3001', '--token=xxx', '--emissionIntervalInMills=300']

    customTest(exp)(args)
    customTest(exp)(splited(args))

    customTest(exp)(toShortForm(args))
    customTest(exp)(toShortForm(splited(args)))

})

test("should use default value 3000 when port is missing", () => {

    const exp = {
        port: 3000,
        token: "xxx",
        emissionIntervalInMills: 300,
    }

    let args = ['--token=xxx', '--emissionIntervalInMills=300']

    customTest(exp)(args)
    customTest(exp)(splited(args))

    customTest(exp)(toShortForm(args))
    customTest(exp)(toShortForm(splited(args)))

})

test("should use default value thisIsTheTokenNow when token is missing", () => {

    const exp = {
        port: 4000,
        token: "thisIsTheTokenNow",
        emissionIntervalInMills: 200,
    }

    let args = ['--port=4000', '--emissionIntervalInMills=200']

    customTest(exp)(args)
    customTest(exp)(splited(args))

    customTest(exp)(toShortForm(args))
    customTest(exp)(toShortForm(splited(args)))

})

test("should use default value 1000 when emissionIntervalInMills is missing", () => {

    const exp = {
        port: 4000,
        token: "xxx",
        emissionIntervalInMills: 500,
    }

    let args = ['--port=4000', '--token=xxx']

    customTest(exp)(args)
    customTest(exp)(splited(args))

    customTest(exp)(toShortForm(args))
    customTest(exp)(toShortForm(splited(args)))

})

function assertMessageContainsGuide(actual) {
    expect(actual.message).toEqual(expect.stringContaining("Starts a mock feed server and as soon a connection is established"))
    expect(actual.message).toEqual(expect.stringContaining("--help"))
    expect(actual.message).toEqual(expect.stringContaining("--port"))
    expect(actual.message).toEqual(expect.stringContaining("--token"))
    expect(actual.message).toEqual(expect.stringContaining("--emissionIntervalInMills"))
}

test("--help return the guide", () => {

    const test = (args) => {
        let actual = loadConfiguration(args);
        expect(actual.isValid).toBe(false)
        expect(actual.config).toBe(null)
        assertMessageContainsGuide(actual)

    }

    test(['--help'])
    test(['-h'])

})

test("An arg that is not recognised produce an invalid configuration", () => {

    let actual = loadConfiguration(['notRecognised']);

    expect(actual.isValid).toBe(false)
    expect(actual.config).toBe(null)
    expect(actual.message).toEqual(expect.stringContaining("Unknown value: notRecognised"))
    assertMessageContainsGuide(actual);

})

test("An option that is not recognised produce an invalid configuration", () => {

    let actual = loadConfiguration(['--notRecognised']);

    expect(actual.isValid).toBe(false)
    expect(actual.config).toBe(null)
    expect(actual.message).toEqual(expect.stringContaining("Unknown option: --notRecognised"))
    assertMessageContainsGuide(actual);

})

