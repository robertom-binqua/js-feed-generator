const {spawn} = require('child_process');
const HttpClient = require('node-rest-client').Client;
const {Manager} = require('socket.io-client');

test('server starts, emits and stops properly with default configuration', done => {

        const expToken = "thisIsTheTokenNow"
        const expPort = "3000"
        const expEmissionIntervalInMills = "500"

        const serverProcessUnderTest = spawn('node', ['./src/server.js']);

        assertThatServerWorks(serverProcessUnderTest, expPort, expToken, expEmissionIntervalInMills, done);

    }
);

test('server starts, emits and stops properly with custom configuration', done => {

    const expToken = "thisIsACustomToken"
    const expPort = "3010"
    const expEmissionIntervalInMills = "100"

    const serverProcessUnderTest = spawn('node', ['./src/server.js', `--port=${expPort}`, `--token=${expToken}`, `--emissionIntervalInMills=${expEmissionIntervalInMills}`]);

    assertThatServerWorks(serverProcessUnderTest, expPort, expToken, expEmissionIntervalInMills, done);

});

test('server print a message if configuration is not valid', done => {

    const notRecognizedValue = "not recognized value"

    const serverProcessUnderTest = spawn('node', ['./src/server.js', notRecognizedValue]);

    let actualCollectedServerStdOutMessages = []

    serverProcessUnderTest.stdout.on('data', (data) => {
        actualCollectedServerStdOutMessages.push(`${data}`)
    });

    setTimeout(() => {

            try {
                expect(serverProcessUnderTest.exitCode).toBe(0)
                expect(actualCollectedServerStdOutMessages.length).toBe(1)
                expect(actualCollectedServerStdOutMessages[0]).toContain(`Unknown value: ${notRecognizedValue}`)
                done()
            } catch (error) {
                done(error);
            }

    }, 500);

});

function assertThatServerWorks(serverProcess, port, theToken, emissionIntervalInMills, done) {

    let actualCollectedServerStdOutMessages = []

    let actualEmittedQuotes = []

    serverProcess.stdout.on('data', (data) => {
        actualCollectedServerStdOutMessages.push(`${data}`)
    });

    const manager = new Manager(`http://localhost:${port}`, {
        autoConnect: true,
        query: {
            access_token: `${theToken}`,
            forceNew: true
        }
    });

    const socketIOClient = manager.socket("/");

    socketIOClient.on('SPX500', function (data) {
        actualEmittedQuotes.push(data.toString())
    });

    setTimeout(() => {

        new HttpClient().post(
            `http://localhost:${port}/subscribe`,
            {
                data: {"pairs": "SPX500"},
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": `Bearer ${socketIOClient.id}${theToken}`,
                    "Content-Language": "en-US",
                    "path": "/subscribe",
                    "port": `${port}`,
                    "accept": "application/json",
                }
            },
            function () {
            }
        );

        setTimeout(() => {

            expect(socketIOClient.connected).toBe(true)

            socketIOClient.close()

            serverProcess.kill("SIGTERM");

            setTimeout(() => {
                try {
                    expect(actualCollectedServerStdOutMessages[0]).toMatch(new RegExp(`server listening on port = ${port} with token = ${theToken} with emissionIntervalInMills = ${emissionIntervalInMills}`))
                    expect(actualCollectedServerStdOutMessages[actualCollectedServerStdOutMessages.length - 1]).toMatch("server stopped by SIGTERM")

                    expect(actualEmittedQuotes.length).toBeGreaterThan(2)
                    expect(actualEmittedQuotes[0]).toMatch(/{"Updated":\d{13},"Rates":\[3000\.50,3001\.00,3100\.50,3000\.00],"Symbol":"SPX500"}/)
                    expect(serverProcess.exitCode).toBe(128 + 15)
                    done()
                } catch (error) {
                    done(error);
                }
            }, 500);

        }, 2500);

    }, 1500);
}



