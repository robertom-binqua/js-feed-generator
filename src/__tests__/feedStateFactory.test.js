const feedStateFactory = require('../feedStateFactory');
const EventEmitter = require('events');

const aLogger = {
    info(string) {
    }
}

test('feedStateFactory can create a connected FeedState', done => {

    const toBeSubscribed = [
        "EUR/GBP",
        "EUR/USD",
        "EUR/JPY",
        "GBP/USD",
        "GBP/JPY",
        "SPX500",
        "USD/JPY",
        "US30",
    ]

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket()

    const currencyPairValueCollector = new Map()

    toBeSubscribed.forEach(currencyPair => {
        currencyPairValueCollector.set(currencyPair, 0)

        mySocket.on(currencyPair, (_) => {
            currencyPairValueCollector.set(currencyPair, currencyPairValueCollector.get(currencyPair) + 1)
        });

    })

    const connectedFeedState = feedStateFactory(aLogger).connectedFeedState('aToken', mySocket, 200)

    toBeSubscribed.forEach(currencyPair => {

        connectedFeedState.validateUrlRequest("/socket.io/?access_token=aToken&EIO=3&transport=websocket&sid=1")

        const subscriptionResult = connectedFeedState.subscribe(currencyPair)

        expect(subscriptionResult.successfulSubscription).toBe(true)
    })

    setTimeout(() => {
        connectedFeedState.stop()
    }, 1000)

    setTimeout(() => {
        toBeSubscribed.forEach(currencyPair => {
            let actualEventsCount = currencyPairValueCollector.get(currencyPair);
            expect(actualEventsCount).toBeGreaterThan(2)
            expect(actualEventsCount).toBeLessThan(8)

        })
        done()
    }, 2200)


})

test('feedStateFactory can create a nonConnected FeedState', () => {

    expect(feedStateFactory(aLogger).nonConnectedFeedState().isConnected).toBe(false)

})

test('Non Connected FeedState does not have socket id', () => {

    expect(() => feedStateFactory(aLogger).nonConnectedFeedState().socketId).toThrowError("Non Connected FeedState does not have socket id")

})

