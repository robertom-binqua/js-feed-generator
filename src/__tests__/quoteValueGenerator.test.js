const quoteValueGenerator = require('../quoteValueGenerator');

test('quote value generator produce right quotes', () => {

    const config = {
        initialValue: "1.1234",
        numberOfValues: 3,
        increment: 1
    }

    const nextQuote = quoteValueGenerator(config)

    expect(nextQuote()).toBe("1.1234")
    expect(nextQuote()).toBe("1.1235")
    expect(nextQuote()).toBe("1.1236")

    expect(nextQuote()).toBe("1.1235")
    expect(nextQuote()).toBe("1.1234")

    expect(nextQuote()).toBe("1.1235")
    expect(nextQuote()).toBe("1.1236")

});

