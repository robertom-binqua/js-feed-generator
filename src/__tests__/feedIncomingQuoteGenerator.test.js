const {feedIncomingQuoteGenerator, quoteUpdatedDecorator, quoteUpdatedDecoratorFactory} = require('../feedIncomingQuoteGenerator');


test('feedIncomingQuoteGenerator validates config argument', () => {

    expect(() => feedIncomingQuoteGenerator({
        sell: 1,
        numberOfValues: "a",
        increment: "a",
        pair: "eur/usd",
    })).toThrowError("increment must be a `number` type, but the final value was: `\"a\"`.");

    expect(() => feedIncomingQuoteGenerator({
        sell: 1,
        numberOfValues: "a",
        increment: 1,
        pair: "eur/usd",
    })).toThrowError("numberOfValues must be a `number` type, but the final value was: `\"a\"`.")

    expect(() => feedIncomingQuoteGenerator({
        sell: "1",
        numberOfValues: 2,
        increment: 1,
        pair: "eur/usd",
    })).toThrowError("sell must match the following: \"/\\d+\\.\\d+/g\"");

    expect(() => feedIncomingQuoteGenerator({
        sell: "1.1",
        numberOfValues: 2,
        increment: 1,
        pair: 2,
    })).toThrowError("pair must be a `string` type, but the final value was: `2`.");


});

test('for EUR/USD, feedIncomingQuoteGenerator produces an entry where max is (numberOfValues + 1) * increment, min is the initial value, buy - sell is equals to increment', () => {

    const config = {
        sell: "1.12300",
        numberOfValues: 5,
        increment: 5,
        pair: "EUR/USD",
    }

    const next = feedIncomingQuoteGenerator(config)

    expect(next()).toBe('{"Rates":[1.12300,1.12305,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12305,1.12310,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12310,1.12315,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12315,1.12320,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12320,1.12325,1.12330,1.12300],"Symbol":"EUR/USD"}')

    expect(next()).toBe('{"Rates":[1.12315,1.12320,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12310,1.12315,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12305,1.12310,1.12330,1.12300],"Symbol":"EUR/USD"}')

    expect(next()).toBe('{"Rates":[1.12300,1.12305,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12305,1.12310,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12310,1.12315,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12315,1.12320,1.12330,1.12300],"Symbol":"EUR/USD"}')
    expect(next()).toBe('{"Rates":[1.12320,1.12325,1.12330,1.12300],"Symbol":"EUR/USD"}')


});

test('for SPX500, feedIncomingQuoteGenerator produces an entry where max is (numberOfValues + 1) * increment, min is the initial value, buy - sell is equals to increment', () => {

    const config = {
        sell: "3025.30",
        numberOfValues: 3,
        increment: 3,
        pair: "SPX500"
    }

    const next = feedIncomingQuoteGenerator(config)

    expect(next()).toBe('{"Rates":[3025.30,3025.33,3025.42,3025.30],"Symbol":"SPX500"}')
    expect(next()).toBe('{"Rates":[3025.33,3025.36,3025.42,3025.30],"Symbol":"SPX500"}')
    expect(next()).toBe('{"Rates":[3025.36,3025.39,3025.42,3025.30],"Symbol":"SPX500"}')

    expect(next()).toBe('{"Rates":[3025.33,3025.36,3025.42,3025.30],"Symbol":"SPX500"}')

    expect(next()).toBe('{"Rates":[3025.30,3025.33,3025.42,3025.30],"Symbol":"SPX500"}')
    expect(next()).toBe('{"Rates":[3025.33,3025.36,3025.42,3025.30],"Symbol":"SPX500"}')
    expect(next()).toBe('{"Rates":[3025.36,3025.39,3025.42,3025.30],"Symbol":"SPX500"}')

});

test('quoteUpdatedDecorator adds the right Updated value', () => {

    const epochGeneratorFactory = () => {
        let start = 123450
        return () => start++
    }

    let decorator = quoteUpdatedDecorator(epochGeneratorFactory());

    expect(decorator('{"a":1}')).toBe('{"Updated":123450,"a":1}')

    expect(decorator('{"a":2}')).toBe('{"Updated":123451,"a":2}')


});

test('quoteUpdatedDecoratorFactory adds a 13 long epoch time', () => {
    expect(quoteUpdatedDecoratorFactory('{"a":2}')).toBeUpdateWith13DigitsLongEpoch()
});

expect.extend({
    toBeUpdateWith13DigitsLongEpoch(received) {
        const updatedValue = JSON.parse(received).Updated;
        const pass = /^\d{13}$/g.test(updatedValue)
        if (pass) {
            return {
                message: () =>
                    `expected an epoch time not to be 13 digits long but got ${updatedValue} in ${received}`,
                pass: true,
            };
        } else {
            return {
                message: () =>
                    `expected an epoch time 13 digits long but got ${updatedValue} in ${received}`,
                pass: false,
            };
        }
    },
});


