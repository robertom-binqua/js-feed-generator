const {upAndDownCounter, formatter, precisionOf, plusPips} = require('../counter');

test('precisionOf works', () => {

    expect(precisionOf("1.12345")).toBe(5)
    expect(precisionOf("1.1234")).toBe(4)
    expect(precisionOf("1.123")).toBe(3)
    expect(precisionOf("1.12")).toBe(2)
    expect(precisionOf("1.1")).toBe(1)
    expect(precisionOf("1.0")).toBe(1)
    expect(precisionOf("1.00")).toBe(2)
    expect(precisionOf("1.000")).toBe(3)
    expect(precisionOf("1")).toBe(0)

});

test('upAndDownCounter works with increment of 1', () => {

    const config = {
        initialValue: "1.1234",
        numberOfValues: 3,
        increment: 1
    }

    const next = upAndDownCounter(config)

    expect(next()).toBe(11234)
    expect(next()).toBe(11235)
    expect(next()).toBe(11236)

    expect(next()).toBe(11235)
    expect(next()).toBe(11234)

    expect(next()).toBe(11235)
    expect(next()).toBe(11236)

    expect(next()).toBe(11235)
    expect(next()).toBe(11234)


});

test('upAndDownCounter works with increment of 2', () => {

    const config = {
        initialValue: "1.1230",
        numberOfValues: 3,
        increment: 2
    }

    const next = upAndDownCounter(config)

    expect(next()).toBe(11230)
    expect(next()).toBe(11232)
    expect(next()).toBe(11234)

    expect(next()).toBe(11232)
    expect(next()).toBe(11230)

    expect(next()).toBe(11232)
    expect(next()).toBe(11234)

});

test('formatter works', () => {

    expect(formatter(12345, 1)).toBe("1234.5")
    expect(formatter(12345, 2)).toBe("123.45")
    expect(formatter(12345, 3)).toBe("12.345")
    expect(formatter(12345, 4)).toBe("1.2345")
    expect(formatter(12345, 5)).toBe("0.12345")
    expect(formatter(12345, 6)).toBe("0.012345")
    expect(formatter(12345, 7)).toBe("0.0012345")

});

test('plusPips works', () => {

    expect(plusPips("1.1311", 100, formatter)).toBe("1.1411")

    expect(plusPips("1.234500", 1, formatter)).toBe("1.234501")
    expect(plusPips("1.234500", 10, formatter)).toBe("1.234510")
    expect(plusPips("1.234500", 100, formatter)).toBe("1.234600")

    expect(plusPips("1.00", 1, formatter)).toBe("1.01")
    expect(plusPips("1.00", 10, formatter)).toBe("1.10")
    expect(plusPips("1.00", 100, formatter)).toBe("2.00")


});


