const immer = require('immer')
const {FeedState} = require('../FeedState');
const EventEmitter = require('events');
const {feedIncomingQuoteGenerator, quoteUpdatedDecorator} = require("../feedIncomingQuoteGenerator")

class MySocket extends EventEmitter {
}

const defaultEntry = {
    sell: "1.12300",
    numberOfValues: 5,
    increment: 5,
    pair: "EUR/USD",
    feedEmissionIntervalMills: 200
}

const defaultConfig = [defaultEntry]

const aLogger = {
    info(string) {
    }
}

test('feedState validation it is wonderful', () => {

    function testEntry(entryUnderTest) {
        return () => new FeedState([entryUnderTest], feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), new MySocket(), aLogger, "someToken");
    }

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.sell = "b"
    }))).toThrowError(Error("[0].sell must match the following: \"/\\d+\\.\\d+/g\""))

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.numberOfValues = "c"
    }))).toThrowError(Error("[0].numberOfValues must be a `number` type, but the final value was: `NaN` (cast from the value `\"c\"`)."))

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.increment = "b"
    }))).toThrowError(Error("[0].increment must be a `number` type, but the final value was: `NaN` (cast from the value `\"b\"`)."))

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.pair = "adc"
    }))).toThrowError(Error("[0].pair must be at least 4 characters"))

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.feedEmissionIntervalMills = "a"
    }))).toThrowError(Error("[0].feedEmissionIntervalMills must be a `number` type, but the final value was: `NaN` (cast from the value `\"a\"`)."))

    expect(testEntry(immer.produce(defaultEntry, newConfigEntry => {
        newConfigEntry.feedEmissionIntervalMills = 1
    }))).toThrowError(Error("[0].feedEmissionIntervalMills must be greater than or equal to 100"))

})

test('socket cannot be null', () => {
    expect(() => new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), null, aLogger, "someToken")).toThrowError(Error("Socket cannot be null"))
})

test('FeedState is connected as soon as it has been created. Once stopped is not connected anymore', () => {

    class MySocket extends EventEmitter {

        get id() {
            return "1234"
        }
    }

    const mySocket = new MySocket()

    let feedState = new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), mySocket, aLogger, "someToken");

    expect(feedState.isConnected).toBe(true)
    expect(feedState.socketId).toBe("1234")

    feedState.stop()

    expect(feedState.isConnected).toBe(false)
    expect(() => feedState.socketId).toThrowError("A stopped feedState does not have a socketId. Please reconnect it")


})

test("url request is is validated if it has pattern /socket.io/?access_token=someloginToken&EIO=3&transport=blablabla&sid=someSocketId where blablabla is websocket or polling", () => {

    class MySocket extends EventEmitter {

        get id() {
            return "1234"
        }
    }

    const mySocket = new MySocket()

    let feedState = new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), mySocket, aLogger, "someToken");

    expect(feedState.validateUrlRequest("")).toBe(false)

    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken1&EIO=3&transport=websocket&sid=1234")).toBe(false)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken1&EIO=3&transport=polling&sid=1234")).toBe(false)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")).toBe(false)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=polling&sid=1")).toBe(false)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=bla&EIO=3&transport=websocket&sid=1234")).toBe(false)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=bla&EIO=3&transport=polling&sid=1234")).toBe(false)

    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1234")).toBe(true)
    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=polling&sid=1234")).toBe(true)

})

test("if feedState has stopped, any url request is not valid", () => {

    class MySocket extends EventEmitter {

        get id() {
            return "1234"
        }
    }

    const mySocket = new MySocket()

    let feedState = new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), mySocket, aLogger, "someToken");

    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1234")).toBe(true)

    feedState.stop()

    expect(feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1234")).toBe(false)

})

test('given a successful subscriptions, FeedState starts emitting quotes if url has been validated', done => {

    const firstCurrencyPair = defaultConfig[0].pair

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket()

    mySocket.once(firstCurrencyPair, (data) => {
        expect(data).toBe(`{"Updated":123451,"Rates":[1.12305,1.12310,1.12330,1.12300],"Symbol":"${firstCurrencyPair}"}`)

        mySocket.once(firstCurrencyPair, (data) => {
            expect(data).toBe(`{"Updated":123452,"Rates":[1.12310,1.12315,1.12330,1.12300],"Symbol":"${firstCurrencyPair}"}`)
            done()
        })
    });

    const epochFactory = () => {
        let start = 123450
        return () => start++
    }

    let feedState = new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(epochFactory()), mySocket, aLogger, "someToken")

    feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")

    let subscriptionResult = feedState.subscribe(firstCurrencyPair)

    expect(subscriptionResult.successfulSubscription).toBe(true)
    expect(subscriptionResult.message).toBe(`{"response":{"executed":true,"error":""},"pairs":[{"Updated":123450,"Rates":[1.12300,1.12305,1.12330,1.12300],"Symbol":"${firstCurrencyPair}"}]}`)

});

test('given a successful subscriptions, FeedState does not emit quotes if url has not been validated', () => {

    const firstCurrencyPair = defaultConfig[0].pair

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket()

    const epochFactory = () => {
        let start = 123450
        return () => start++
    }

    let feedState = new FeedState(defaultConfig, feedIncomingQuoteGenerator, quoteUpdatedDecorator(epochFactory()), mySocket, aLogger, "someToken")

    feedState.validateUrlRequest("non valid url")

    let subscriptionResult = feedState.subscribe(firstCurrencyPair)

    expect(subscriptionResult.successfulSubscription).toBe(false)
    expect(subscriptionResult.message).toBe("url has not been validated")

});

const feedEmissionIntervalMillsTester = (done, feedEmissionIntervalMills, expNumberOfEventsLessThan) => {
    const currencyPair = "EUR/USD"

    const config = [
        {
            sell: "1.12300",
            numberOfValues: 5,
            increment: 5,
            pair: `${currencyPair}`,
            feedEmissionIntervalMills: feedEmissionIntervalMills
        }
    ]

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const eurUsdCollector = []

    const mySocket = new MySocket()

    mySocket.on(currencyPair, data => {
        eurUsdCollector.push(data)
    })

    let feedState = new FeedState(config, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), mySocket, aLogger, "someToken")

    feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")

    expect(feedState.subscribe(currencyPair).successfulSubscription).toBe(true)

    let enoughToCollectLowerThanMinus1Events = feedEmissionIntervalMills * (expNumberOfEventsLessThan - 1) + feedEmissionIntervalMills / 2;

    setTimeout(() => {
        feedState.stop()
        expect(feedState.isStopped()).toBe(true)
    }, enoughToCollectLowerThanMinus1Events)

    setTimeout(() => {
        expect(eurUsdCollector.length).toBeGreaterThanOrEqual(expNumberOfEventsLessThan - 1)
        expect(eurUsdCollector.length).toBeLessThan(expNumberOfEventsLessThan)
        done()
    }, (expNumberOfEventsLessThan * feedEmissionIntervalMills))
}

describe('feedEmissionIntervalMills is taken into account', () => {
    test('feedEmissionIntervalMills of 600 works', done => {

        feedEmissionIntervalMillsTester(done, 600, 3)

    })
    test('feedEmissionIntervalMills of 300 works', done => {

        feedEmissionIntervalMillsTester(done, 300, 6)

    })
})

test('given a wrong currency pair, then FeedState cannot subscribe it and cannot starts emitting quotes', done => {

    const notConfigCurrencyPair = "BlaBla"

    const config = [
        {
            sell: "1.12300",
            numberOfValues: 5,
            increment: 5,
            pair: "EUR/USD",
            feedEmissionIntervalMills: 200
        }
    ]

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket()

    mySocket.once(notConfigCurrencyPair, () => {
        throw Error("We should not arrive at this point ;)")
    });

    const feedState = new FeedState(
        config,
        feedIncomingQuoteGenerator,
        quoteUpdatedDecorator(() => {
            throw Error("We should not arrive at this point")
        }),
        mySocket,
        aLogger,
        "someToken"
    )

    feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")

    const subscriptionResult = feedState.subscribe(notConfigCurrencyPair)

    expect(subscriptionResult.successfulSubscription).toBe(false)
    expect(subscriptionResult.message).toBe(`Currency pair ${notConfigCurrencyPair} is not configured`)

    setTimeout(() => done(), 1000)

})

test('after stop all emitters are stopped', done => {

    const feedEmissionIntervalMills = 500

    const maxNumberOfEvents = 4

    const config = [
        {
            sell: "1.12300",
            numberOfValues: 5,
            increment: 5,
            pair: "EUR/USD",
            feedEmissionIntervalMills: `${feedEmissionIntervalMills}`
        },
        {
            sell: "3025.30",
            numberOfValues: 3,
            increment: 3,
            pair: "SPX500",
            feedEmissionIntervalMills: `${feedEmissionIntervalMills}`
        }

    ]

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket()

    const eurUsdCollector = []

    mySocket.on("EUR/USD", data => {
        eurUsdCollector.push(data)
    })

    const spx500Collector = []

    mySocket.on("SPX500", data => {
        spx500Collector.push(data)
    })

    const epochFactory = () => {
        let start = 123450
        return () => start++
    }

    let feedState = new FeedState(config, feedIncomingQuoteGenerator, quoteUpdatedDecorator(epochFactory()), mySocket, aLogger, "someToken")

    feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")

    expect(feedState.isStopped()).toBe(false)

    feedState.subscribe("EUR/USD")
    feedState.subscribe("SPX500")

    let enoughTimeToGenerateMaxNumberOfEventMinus1 = feedEmissionIntervalMills * maxNumberOfEvents - feedEmissionIntervalMills / 2;

    setTimeout(() => {
        feedState.stop()
        expect(feedState.isStopped()).toBe(true)
    }, enoughTimeToGenerateMaxNumberOfEventMinus1)

    setTimeout(() => {
        expect(eurUsdCollector.length).toBeGreaterThanOrEqual(maxNumberOfEvents - 1)
        expect(eurUsdCollector.length).toBeLessThan(maxNumberOfEvents)

        expect(spx500Collector.length).toBeGreaterThanOrEqual(maxNumberOfEvents - 1)
        expect(spx500Collector.length).toBeLessThan(maxNumberOfEvents)
        done()
    }, feedEmissionIntervalMills * (maxNumberOfEvents + 1))

});

test('after disconnect we cannot subscribe again to any currencyPair', () => {

    const config = [
        {
            sell: "1.12300",
            numberOfValues: 5,
            increment: 5,
            pair: "EUR/USD",
            feedEmissionIntervalMills: 200
        },
        {
            sell: "3025.30",
            numberOfValues: 3,
            increment: 3,
            pair: "SPX500",
            feedEmissionIntervalMills: 200
        }

    ]

    class MySocket extends EventEmitter {
        get id() {
            return "1"
        }
    }

    const mySocket = new MySocket(
    )

    mySocket.on("EUR/USD", () => {
    })

    mySocket.on("SPX500", () => {
    })

    let feedState = new FeedState(config, feedIncomingQuoteGenerator, quoteUpdatedDecorator(() => 1), mySocket, aLogger, "someToken");

    feedState.validateUrlRequest("/socket.io/?access_token=someToken&EIO=3&transport=websocket&sid=1")

    feedState.subscribe("EUR/USD")

    feedState.stop()

    expect(() => feedState.subscribe("SPX500")).toThrowError("Feed has been stopped!")

});


