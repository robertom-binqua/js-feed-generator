const {FeedState} = require("./FeedState")
const {feedIncomingQuoteGenerator, quoteUpdatedDecoratorFactory} = require("./feedIncomingQuoteGenerator")

const internalFeedStateFactory = (token, socket, feedEmissionIntervalMills, feedStateLogger) => {

    const defaultConfig = {
        numberOfValues: 200,
        increment: 50,
        feedEmissionIntervalMills: feedEmissionIntervalMills
    }

    const config = [
        {
            sell: "0.89000",
            pair: "EUR/GBP",
            ...defaultConfig,
        },
        {
            sell: "121.900",
            pair: "EUR/JPY",
            ...defaultConfig,
        },
        {
            sell: "1.12300",
            pair: "EUR/USD",
            ...defaultConfig,
        },
        {
            sell: "1.26200",
            pair: "GBP/USD",
            ...defaultConfig,
        },
        {
            sell: "135.100",
            pair: "GBP/JPY",
            ...defaultConfig,
        },
        {
            sell: "3000.00",
            pair: "SPX500",
            ...defaultConfig,
        },
        {
            sell: "107.000",
            pair: "USD/JPY",
            ...defaultConfig,
        },
        {
            sell: "26500.00",
            pair: "US30",
            ...defaultConfig,
        }
    ]

    return new FeedState(config, feedIncomingQuoteGenerator, quoteUpdatedDecoratorFactory, socket, feedStateLogger, token)
}


const feedStateFactory = (feedStateLogger) => {
    return ({
        nonConnectedFeedState() {
            return {
                get isConnected() {
                    return false
                },
                get socketId() {
                    throw Error("Non Connected FeedState does not have socket id")
                }


            }
        },
        connectedFeedState(token , socket, feedEmissionIntervalMills) {
            return internalFeedStateFactory(token, socket, feedEmissionIntervalMills, feedStateLogger)
        }
    })
}

module.exports = feedStateFactory
