FROM node:12.18.4-alpine

WORKDIR /usr/

COPY package*.json ./

RUN npm install --production

COPY . .

WORKDIR /usr/src

EXPOSE 3000

CMD exec node server.js -p ${EXTERNAL_FEED_PORT:-3000} -t ${EXTERNAL_FEED_TOKEN:-thisIsTheTokenNow} -i ${EXTERNAL_FEED_EMISSION_INTERVAL_IN_MS:-4000}
